Para preparar un proyecto en python con django debemos tener la version 3.x.x de python
*Recomiendo usar la 3.6.5

Muy bien, despues de instalar python 3 comprueba que este corriendo en tu terminal
```
python3 --version o python --version
```
Primero crea tu folder, django-init
comprueba que la versión que muestre la terminal sea 3.x.x

Ya instalado lo básico necesitamos crear nuestro entorno virtual de python para no tener algún choque con nuestras dependencias por decir,
tenemos un proyecto que usamos python como ML y otro una api, si instalamos nuestros paquetes o bibliotecas de forma global sin crear un virtualenv
de python es muy probable que algo no funcione, para es mejor separarlo

para esto podemos ejecutar este comando

```
python3 -m venv .env el punto nos dice que será una carpeta oculta
```

esto nos crea el ambiente virtual, .env es opcional puedes nombrarlo como quieras. Ejemplo .WRY

para activar nuestro entorno virtual necesitamos usar el siguiente comando:

```
source .env/bin/activate (recordar, .env es el nombre que le dimos)
```

oka, ya que lo tenemos listo pasaremos a instalar django

```
pip install django -U -> para tener la última versión
```

luego usamos esto para iniciar django
usamos django-admin startproject pythonprros .

** nota si no ponemos el punto, nos creara una carpeta que se llamará pythonprros, el punto nos dice que va iniciar el projecto
en la raiz de nuestra carpeta que sería django-init, con esto evitamos que nos cree una subcarpeta llamada pythonprros

pip freeze nos dirá que cosas tenemos instalado en nuestro projecto de django pero con base al entorno virutal que tenemos activado
esto es lo magico de django, evitamos que mezclemos bibliotecas, paquetes en otros projectos donde usamos otras versiones

y ya para finalizar usa ```code . ``` esto abrirá el projecto en visual studio code, bueno si usas el vsc

```
## Windows
# Crear entorno virtual
> python -m venv .env
# Activar entono virtual
> .env/Scripts/activate
# Desactivar entorno virtual
> .env/Scripts/deactivate

## Ubuntu
# Crear entorno virtual
$ python3 -m venv .env
# Activar entono virtual
$ source .env/bin/activate
# Desactivar entorno virtual
$ deactivate

## Mac
# Crear entorno virtual
python3 -m venv .env
# Activar entono virtual
source .env/bin/activate
# Desactivar entorno virtual
deactivate
```


